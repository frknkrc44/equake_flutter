import 'package:equake/models/base.dart';

class KandilliEarthQuake extends BaseEarthQuake {
  final String konum;

  KandilliEarthQuake({
    String tarih,
    String saat,
    String enlem,
    String boylam,
    String derinlik,
    String siddet,
    this.konum,
  }) : super(
          tarih: tarih,
          saat: saat,
          enlem: enlem,
          boylam: boylam,
          derinlik: derinlik,
          siddet: siddet,
        );

  @override
  String getLocation() {
    return konum;
  }
}
