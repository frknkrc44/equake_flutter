import 'package:equake/models/base.dart';

class AFADEarthQuake extends BaseEarthQuake {
  final String rms, tip, ulke, il, ilce, koy, diger;

  AFADEarthQuake({
    String tarih,
    String saat,
    String enlem,
    String boylam,
    String derinlik,
    this.rms,
    this.tip,
    String buyukluk,
    this.ulke,
    this.il,
    this.ilce,
    this.koy,
    this.diger,
  }) : super(
          tarih: tarih.split(' ')[0].replaceAll('-', '.'),
          saat: tarih.split(' ')[1].replaceAll('Z', ''),
          enlem: enlem,
          boylam: boylam,
          derinlik: derinlik,
          siddet: buyukluk,
        );

  @override
  String getLocation() {
    String out = '';
    for (var item in [koy, ilce, il, ulke, diger]) {
      if (item.trim() != '-') {
        out += '$item, ';
      }
    }
    out = out.substring(0, out.length - 2);
    return out;
    // return '$koy, $ilce, $il, $ulke, $diger';
  }
}
