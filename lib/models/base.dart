import 'dart:convert';
export 'afad/earthquake.dart';
export 'kandilli/earthquake.dart';

enum EarthQuakeInfoProvider {
  AFAD,
  KANDILLI,
}

class BaseEarthQuake {
  final String tarih, saat, enlem, boylam, derinlik, siddet;

  BaseEarthQuake({
    this.tarih,
    this.saat,
    this.enlem,
    this.boylam,
    this.derinlik,
    this.siddet,
  });

  String getLocation() {
    throw UnimplementedError();
  }

  Map<String, dynamic> asMap() => {
        'tarih': tarih,
        'saat': saat,
        'enlem': enlem,
        'boylam': boylam,
        'derinlik': derinlik,
        'siddet': siddet,
        'konum': getLocation(),
      };

  @override
  String toString() => jsonEncode(asMap());
}
