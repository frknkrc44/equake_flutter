import 'package:equake/models/base.dart';
import 'package:equake/network/base.dart';
import 'package:flutter/cupertino.dart';

class AFADConnection extends BaseConnection {
  AFADConnection()
      : super(
          'https://deprem.afad.gov.tr/latestCatalogsExport',
          EarthQuakeInfoProvider.AFAD,
        );

  @override
  Future<List<BaseEarthQuake>> getList(BuildContext context) async {
    List<BaseEarthQuake> out = [];

    try {
      var parsedBody = getHTTPParsedBody({
        'searchedMInput': 0,
        'searchedUtcInput': 0,
        'searchedLastDayInput': 7,
        'exportType': 2,
      });

      var result = await getHttpResponse(
        context: context,
        method: 'POST',
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        body: parsedBody,
      );

      if (result == null || result.statusCode != 200) {
        print('AFAD failed');
        return out;
      }

      var decoded = await getDecodedBytes(result);
      var split = decoded.split('\n');
      bool firstItem = true;
      for (var item in split) {
        if (firstItem) {
          firstItem = false;
          continue;
        }
        var temp = item.trim();
        var deprem = temp.split(',');
        var koy = deprem[12];
        var diger = temp.substring(temp.lastIndexOf(koy) + koy.length, temp.length).trim();
        diger = diger.length > 1 ? diger.substring(1, diger.length) : '-';
        out.add(AFADEarthQuake(
          tarih: deprem[2],
          enlem: deprem[3],
          boylam: deprem[4],
          derinlik: '${deprem[5]}km',
          rms: deprem[6],
          tip: deprem[7],
          buyukluk: deprem[8],
          ulke: deprem[9],
          il: deprem[10],
          ilce: deprem[11],
          koy: koy,
          diger: diger,
        ));
      }
    } catch (e) {}

    return out;
  }
}
