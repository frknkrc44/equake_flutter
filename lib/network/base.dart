import 'dart:convert';

import 'package:equake/main.dart';
import 'package:equake/models/base.dart';
import 'package:equake/network/afad.dart';
import 'package:equake/network/kandilli.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:connectivity/connectivity.dart';
import 'package:charset_converter/charset_converter.dart';

class EarthQuakeConnectionProvider {
  final Map<String, BaseConnection> _providers = {
    'AFAD': AFADConnection(),
    'Kandilli': KandilliConnection(),
  };

  List<String> get names => _providers.keys.toList();

  BaseConnection getFromName(EarthQuakeInfoProvider name) {
    return _providers[getProviderNameStr(name)];
  }

  String getProviderNameStr(EarthQuakeInfoProvider provider) {
    switch (provider) {
      case EarthQuakeInfoProvider.AFAD:
        return _providers.keys.first;
      case EarthQuakeInfoProvider.KANDILLI:
        return _providers.keys.last;
    }
    return 'UNKNOWN';
  }

  EarthQuakeInfoProvider getProviderName(String str) {
    return _providers[str]?.provider;
  }
}

class BaseConnection {
  final String url;
  final EarthQuakeInfoProvider provider;

  BaseConnection(this.url, this.provider);

  Future<List<BaseEarthQuake>> getList(BuildContext context) async {
    throw UnimplementedError();
  }

  Future<http.Response> getHttpResponse({
    @required BuildContext context,
    @required String method,
    Map<String, String> headers,
    dynamic body,
  }) async {
    if (await isNotConnected(context)) {
      return null;
    }

    dynamic bodyStr;
    var reqHeaders = {
      "User-Agent":
          "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) brave/0.7.10 Chrome/47.0.2526.110 Brave/0.36.5 Safari/537.36",
    };

    if (headers != null) {
      for (var i in headers.keys) {
        if (reqHeaders.containsKey(i)) {
          reqHeaders.remove(i);
        }
        reqHeaders.putIfAbsent(i, () => headers[i]);
      }
    }

    if (body != null && body is Map && bodyStr == null) {
      bodyStr = jsonEncode(body);
      print(bodyStr);
    }

    switch (method) {
      case 'GET':
        return await http.get(url, headers: reqHeaders);
      case 'POST':
        return await http.post(url, headers: reqHeaders, body: bodyStr ?? body);
    }
    return null;
  }

  String getHTTPParsedBody(Map<String, dynamic> items) {
    String out = '';
    for (var item in items.keys) {
      out += '$item=${items[item]}&';
    }
    out = out.substring(0, out.length - 1);
    return out;
  }

  int _latestNetworkErrorTime = 0;

  Future<bool> isConnected(BuildContext context) async {
    try {
      var result = (await MyApp.connectivity.checkConnectivity()) ?? ConnectivityResult.none;
      if (result == ConnectivityResult.none) {
        var date = DateTime.now();
        if ((date.millisecondsSinceEpoch - _latestNetworkErrorTime) > 5000) {
          _latestNetworkErrorTime = date.millisecondsSinceEpoch;
          print('Network is not connected yet');
        }
        return false;
      }
      return true;
    } catch (e) {
      return false;
    }
  }

  Future<bool> isNotConnected(BuildContext context) async => !await isConnected(context);

  Future<String> getDecodedBytes(http.Response response, {String encoding: 'ISO-8859-9'}) async {
    if (encoding == null) {
      return response.body;
    }

    return await CharsetConverter.decode(encoding, response.bodyBytes);
  }
}
