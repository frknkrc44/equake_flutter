import 'package:equake/models/base.dart';
import 'package:equake/network/base.dart';
import 'package:flutter/material.dart';

class KandilliConnection extends BaseConnection {
  KandilliConnection()
      : super(
          'http://www.koeri.boun.edu.tr/scripts/LST1.asp',
          EarthQuakeInfoProvider.KANDILLI,
        );

  @override
  Future<List<BaseEarthQuake>> getList(BuildContext context) async {
    List<BaseEarthQuake> out = [];

    try {
      var result = await getHttpResponse(context: context, method: 'GET');

      if (result == null || result.statusCode != 200) {
        return out;
      }

      var decoded = (await getDecodedBytes(result)).replaceAll('\r', '');

      if (!decoded.contains('<pre>')) {
        return out;
      }

      decoded = decoded.substring(decoded.indexOf('<pre>') + 5, decoded.indexOf('</pre>')).trim();
      var split = decoded.split('\n');
      bool lineFound = false;
      for (var item in split) {
        if (lineFound && item.trim().isNotEmpty) {
          item = item.replaceAll(RegExp(r'\s+'), ' ').trim();
          var split = item.split(' ');
          String konum = item.substring(item.lastIndexOf(split[7]) + split[7].length + 1);
          if (konum.endsWith('İlksel')) {
            konum = konum.replaceFirst('İlksel', '');
          } else {
            konum = konum.substring(0, konum.indexOf(split[split.length - 3]));
          }
          out.add(KandilliEarthQuake(
            tarih: split[0],
            saat: split[1],
            enlem: split[2],
            boylam: split[3],
            derinlik: '${split[4]}km',
            siddet: split[6],
            konum: konum.trim(),
          ));
        } else if (item.trim().startsWith('---')) {
          lineFound = true;
        }
      }
    } catch (e, s) {
      print(e);
      print(s);
    }

    return out;
  }
}
