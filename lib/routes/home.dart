import 'package:equake/main.dart';
import 'package:equake/models/base.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class HomePage extends StatefulWidget {
  HomePage({Key key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  EarthQuakeInfoProvider _selectedProvider = EarthQuakeInfoProvider.AFAD;
  List<BaseEarthQuake> _itemCache;

  @override
  void initState() {
    super.initState();
    Future.doWhile(() async {
      if (_selectedProvider != null) {
        _itemCache = null;
        setState(() {});
      }
      await Future.delayed(Duration(minutes: 15));
      return true;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('EQUAKE'),
        actions: <Widget>[
          DropdownButtonHideUnderline(
            child: DropdownButton(
              hint: Text('Provider'),
              value: _selectedProvider,
              onChanged: (e) {
                if (e != _selectedProvider) {
                  _selectedProvider = e;
                  _itemCache = null;
                  setState(() {});
                }
              },
              items: <DropdownMenuItem>[
                ...MyApp.providers.names.map(
                  (e) => DropdownMenuItem(
                    child: Text(e),
                    value: MyApp.providers.getProviderName(e),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
      extendBody: true,
      body: _itemCache == null
          ? FutureBuilder(
              future: MyApp.providers.getFromName(_selectedProvider).getList(context),
              builder: (BuildContext context, snapshot) {
                if (snapshot.data != null) {
                  _itemCache = snapshot.data;
                  return body;
                }

                return Container(
                  width: double.infinity,
                  height: double.infinity,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      CircularProgressIndicator(),
                    ],
                  ),
                );
              },
            )
          : body,
    );
  }

  Widget get body => Scrollbar(
        child: ListView.builder(
          itemCount: _itemCache.length,
          itemBuilder: (BuildContext context, int index) {
            var item = _itemCache[index];
            return ListTile(
              title: Text(item.getLocation()),
              subtitle: Text('${item.siddet} - ${item.derinlik} - ${(item.saat != null ? item.saat + ' ' : '') + item.tarih}'),
              onTap: () async {
                var url = 'https://maps.google.com/?q=${item.enlem},${item.boylam}';
                if (await canLaunch(url)) {
                  await launch(url);
                }
              },
            );
          },
        ),
      );
}
